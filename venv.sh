#!/usr/bin/env bash

readonly VENV_DIR=_venv_python_osmltupd

(return 0 2>/dev/null) && SOURCED=1 || SOURCED=0

if [[ $SOURCED == 0 ]]
then
    >&2 echo "Use 'source venv.sh' to work within venv"
    exit 1
fi

[[ ! -e "${VENV_DIR}" ]] && python3 -m venv "${VENV_DIR}"

source _venv_python_osmltupd/bin/activate
pip install -r requirements.txt
