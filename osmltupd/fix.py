#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List

import osmltupd.consts
import osmltupd.geo_utils
import osmltupd.format
from osmltupd.report import MissingAvedrageSpeedCamReportItem

from speed_cams import OsmSpeedCam
from video_cams import OfficialTrafficVideoCam


def fix_incorrectly_tagged_video_cams_as_speed_cams_in_osm_with_josm(incorrectly_tagged_report: List):
    
    for index, item in enumerate(incorrectly_tagged_report):
        print('=' * 16)
        print('Fixing {}/{} error'.format(index + 1, len(incorrectly_tagged_report)))
        print('Official id: {}'.format(item.official_match.id))
        print('Official coordinates: {},{}'.format(item.official_match.lat, item.official_match.lng))
        print('OSM match coordinates: {},{}'.format(item.osm_match.lat, item.osm_match.lng))
        print('Match distance: {:.1f}m'.format(osmltupd.geo_utils.distance(item.official_match.coordinate, item.osm_match.coordinate)))
        print('OSM id: {}'.format(item.osm_match.id))
        print('OSM url: {}'.format(osmltupd.format.osm_link_for_node_id(item.osm_match.id)))
        print('JOSM load url: {}'.format(osmltupd.format.josm_link_for_loading_node(item.osm_match.id, item.osm_match.coordinate)))
        print('JOSM suggested tags to use:\n{}\n'.format(osmltupd.format.josm_text_tag_list(osmltupd.consts.SUGGESTED_JOSM_TRAFFIC_CONDITIONS_VIDEO_CAM_TAGS)))
        input('Press Enter to continue...')
        

def fix_missing_speed_cams_in_osm_with_josm(official_camera_list: List):
    
    for index, item in enumerate(official_camera_list):
        print('=' * 16)
        print('Adding {}/{} camera'.format(index + 1, len(official_camera_list)))
        print('Official id: {}'.format(item.id))
        print('Official coordinates: {},{}'.format(item.lat, item.lng))
        print('Official speed: {}'.format(item.speed if item.speed else "Unknown"))
        final_tags = dict(osmltupd.consts.SUGGESTED_JOSM_SPEED_CAM_TAGS)
        if item.speed:
            final_tags['maspeed'] = str(item.speed)
        print('JOSM add url: {}'.format(osmltupd.format.josm_link_for_adding_node(item.coordinate, final_tags)))
        input('Press Enter to continue...')


def fix_missing_average_speed_cams_in_osm_with_josm(official_range_list: List[MissingAvedrageSpeedCamReportItem]):
    
    for index, item in enumerate(official_range_list):
        first = item.range.first
        second = item.range.second
        print('=' * 16)
        print('Fixing {}/{} average speed range'.format(index + 1, len(official_range_list)))
        print('Official range title: {}'.format(first.range_title))
        
        print('-- First camera --')
        print('Official id: {}'.format(second.id))
        print('Official coordinates: {},{}'.format(first.lat, first.lng))
        if item.first_missing:
            final_tags = dict(osmltupd.consts.SUGGESTED_JOSM_AVERAGE_SPEED_CAM_TAGS)
            print('JOSM add url: {}'.format(osmltupd.format.josm_link_for_adding_node(first.coordinate, final_tags)))
        else:
            print('JOSM view url: {}'.format(osmltupd.format.josm_link_for_loading_area(first.coordinate)))
            
        print('-- Second camera --')
        print('Official id: {}'.format(second.id))
        print('Official coordinates: {},{}'.format(second.lat, second.lng))    
        if item.second_missing:        
            final_tags = dict(osmltupd.consts.SUGGESTED_JOSM_AVERAGE_SPEED_CAM_TAGS)
            print('JOSM add url: {}'.format(osmltupd.format.josm_link_for_adding_node(second.coordinate, final_tags)))
        else:            
            print('JOSM view url: {}'.format(osmltupd.format.josm_link_for_loading_area(second.coordinate)))
                        
        input('Press Enter to continue...')


def fix_missing_traffic_conditions_video_cams_in_osm_with_josm(official_camera_list: List):
    
    for index, item in enumerate(official_camera_list):
        print('=' * 16)
        print('Adding {}/{} camera'.format(index + 1, len(official_camera_list)))
        print('Official id: {}'.format(item.id))
        print('Official coordinates: {},{}'.format(item.lat, item.lng))
        final_tags = dict(osmltupd.consts.SUGGESTED_JOSM_TRAFFIC_CONDITIONS_VIDEO_CAM_TAGS)
        print('JOSM add url: {}'.format(osmltupd.format.josm_link_for_adding_node(item.coordinate, final_tags)))
        input('Press Enter to continue...')


def fix_redundant_speed_cams_in_osm_with_josm(redundant_cam_list: List):
    
    for index, item in enumerate(redundant_cam_list):
        print('WARNING: Not all cameras are available in eismoinfo.lt!')
        print('WARNING: Use this as a hint only for roads of national importance! Check Mapillary/Kartaview if possible before removing the camera!')
        print('Viewing {}/{} redundant camera'.format(index + 1, len(redundant_cam_list)))
        print('OSM id: {}'.format(item.id))
        print('OSM url: {}'.format(osmltupd.format.osm_link_for_node_id(item.id)))
        print('JOSM load url: {}'.format(osmltupd.format.josm_link_for_loading_node(item.id, item.coordinate)))
        input('Press Enter to continue...')

        