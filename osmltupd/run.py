#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import osmltupd.fetch
import osmltupd.fix
import osmltupd.printing
import osmltupd.report
import osmltupd.speed_cams


def run_traffic_conditions_video_cams_as_speed_cams_command(arguments):
    
    print('Fetching Lithuanian speed cameras from OSM...')
    osm_list = osmltupd.fetch.fetch_lithuanian_speed_camera_nodes_from_osm_or_cache(use_cache = arguments.cache)

    print('Fetching Lithuanian traffic conditions video cameras from eismoinfo.lt...')
    official_video_cam_list = osmltupd.fetch.fetch_lithuanian_traffic_conditions_video_cameras_from_eismoinfo_lt()
    
    print('Fetching Lithuanian speed cameras from eismoinfo.lt...')
    official_speed_camera_list = osmltupd.fetch.fetch_lithuanian_speed_cameras_from_eismoinfo_lt()
    
    print('Fetching Lithuanian average speed cameras from eismoinfo.lt...')
    official_average_speed_camera_list = osmltupd.fetch.fetch_lithuanian_average_speed_cameras_from_eismoinfo_lt()
    
    print('Total official video camera count: {}'.format(len(official_video_cam_list)))
    print('Total OSM camera count: {}'.format(len(osm_list)))
    
    print('Generating traffic conditions video cams as speed cams mistake report...')
    incorrectly_tagged_list = osmltupd.report.report_incorrectly_tagged_traffic_conditions_video_cams_as_speed_cams_in_osm(official_video_cam_list, official_speed_camera_list, official_average_speed_camera_list, osm_list)
    
    count = len(incorrectly_tagged_list)
    print('Incorrectly tagged video cameras as speed camera count: {}'.format(count))
    
    if not count:
        return 
    
    if arguments.fix_with_josm:        
        osmltupd.fix.fix_incorrectly_tagged_video_cams_as_speed_cams_in_osm_with_josm(incorrectly_tagged_list)
    else:
        osmltupd.printing.print_plaintext_list_report('Incorreclty tagged video cameras as speed cameras:', incorrectly_tagged_list)


def run_missing_traffic_conditions_video_cams_command(arguments):
    
    print('Fetching Lithuanian traffic conditions (traffic surveillance) video cameras from OSM...')
    osm_list = osmltupd.fetch.fetch_lithuanian_traffic_surveillance_camera_nodes_from_osm_or_cache(use_cache = arguments.cache)

    print('Fetching Lithuanian traffic conditions video cameras from eismoinfo.lt...')
    official_list = osmltupd.fetch.fetch_lithuanian_traffic_conditions_video_cameras_from_eismoinfo_lt()
    
    print('Total official video camera count: {}'.format(len(official_list)))
    print('Total OSM camera count: {}'.format(len(osm_list)))
    
    print('Generating missing traffic conditions video cameras report...')
    missing_list = osmltupd.report.report_missing_traffic_conditions_video_cameras_in_osm(official_list, osm_list)
    
    count = len(missing_list)
    print('Missing traffic conditions video camera count: {}'.format(count))
    
    if not count:
        return;
    
    if arguments.fix_with_josm:        
        osmltupd.fix.fix_missing_traffic_conditions_video_cams_in_osm_with_josm(missing_list)
    else:
        osmltupd.printing.print_plaintext_list_report('Missing traffic conditions video cameras:', missing_list)


def run_missing_speed_cams_command(arguments):   
              
    print('Fetching Lithuanian speed cameras from OSM...')
    osm_list = osmltupd.fetch.fetch_lithuanian_speed_camera_nodes_from_osm_or_cache(use_cache = arguments.cache)

    print('Fetching Lithuanian speed cameras from eismoinfo.lt...')
    official_list = osmltupd.fetch.fetch_lithuanian_speed_cameras_from_eismoinfo_lt()

    print('Total OSM camera count: {}'.format(len(osm_list)))
    print('Official speed camera count: {}'.format(len(official_list)))
        
    print('Generating missing camera report...')
    missing_list = osmltupd.report.report_missing_speed_cams_in_osm(official_list, osm_list)
    
    count = len(missing_list)
    print('Missing camera count: {}'.format(count))
    
    if not count:
        return
    
    if arguments.fix_with_josm:
        osmltupd.fix.fix_missing_speed_cams_in_osm_with_josm(missing_list)
    else:
        osmltupd.printing.print_plaintext_list_report('Missing cameras:', missing_list)


def run_missing_average_speed_cams_command(arguments):   
              
    print('Fetching Lithuanian speed cameras from OSM...')
    osm_list = osmltupd.fetch.fetch_lithuanian_speed_camera_nodes_from_osm_or_cache(use_cache = arguments.cache)

    print('Fetching Lithuanian average speed cameras from eismoinfo.lt...')
    official_list = osmltupd.fetch.fetch_lithuanian_average_speed_cameras_from_eismoinfo_lt()

    print('Total OSM speed camera (instantenious and average speed) count: {}'.format(len(osm_list)))
    print('Official average speed camera count: {}'.format(len(official_list)))
        
    print('Generating missing average speed camera report...')
    missing_list = osmltupd.report.report_missing_average_speed_cams_in_osm(official_list, osm_list)
    
    count = len(missing_list)
    print('Missing or incomplete average speed camera range count: {}'.format(count))
    
    if not count:
        return
    
    if arguments.fix_with_josm:
        osmltupd.fix.fix_missing_average_speed_cams_in_osm_with_josm(missing_list)
    else:
        osmltupd.printing.print_plaintext_list_report('Missing or incomplete average speed camera ranges:', missing_list)


def run_redundant_speed_cams_command(arguments):
    
    print('Fetching Lithuanian speed cameras from OSM...')
    osm_list = osmltupd.fetch.fetch_lithuanian_speed_camera_nodes_from_osm_or_cache(use_cache = arguments.cache)

    print('Fetching Lithuanian instant speed cameras from eismoinfo.lt...')
    official_instant_cam_list = osmltupd.fetch.fetch_lithuanian_speed_cameras_from_eismoinfo_lt()
    
    print('Fetching Lithuanian average speed cameras from eismoinfo.lt...')
    official_average_cam_list = osmltupd.fetch.fetch_lithuanian_average_speed_cameras_from_eismoinfo_lt()
    
    print('Generating redundant camera report (please fix "traffic-conditions-video-cams-as-speed-cams" mistakes first!)...')
    redundant_list = osmltupd.report.report_redundant_speed_cams_in_osm(official_instant_cam_list, official_average_cam_list, osm_list)
    
    count = len(redundant_list)
    print('Possibly redundant camera count: {}'.format(count))
    
    if not count:
        return
    
    if arguments.fix_with_josm:
        osmltupd.fix.fix_redundant_speed_cams_in_osm_with_josm(redundant_list)
    else:
        print('WARNING: Not all cameras are available in eismoinfo.lt!')
        print('WARNING: Use this as a hint only for roads of national importance! Check Mapillary/Kartaview if possible before removing the camera!')
        osmltupd.printing.print_plaintext_list_report('Possibly redundant cameras:', redundant_list)
       
