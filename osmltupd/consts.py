#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

EISMOINFO_SPEED_CAMERA_JSON_URL = 'https://gis.ktvis.lt/arcgis/rest/services/PUB/PUB_ITS/MapServer/12/query?outFields=*&where=1%3D1&f=pjson'
EISMOINFO_AVERAGE_SPEED_CAMERA_JSON_URL = 'https://gis.ktvis.lt/arcgis/rest/services/PUB/PUB_ITS/MapServer/17/query?outFields=*&where=1%3D1&f=pjson'
EISMOINFO_TRAFFIC_CONDITIONS_VIDEO_CAMERA_JSON_URL = 'https://gis.ktvis.lt/arcgis/rest/services/PUB/PUB_ITS/MapServer/3/query?outFields=*&where=1%3D1&f=pjson'
OSM_SPEED_CAMERA_CACHE_FILE_NAME = 'osmltupd_osm_speed_cameras_cache.json'
OSM_SURVEILLANCE_VIDEO_CAMS_CACHE_FILE_NAME = 'osmltupd_osm_surveillance_video_cameras_cache.json'
DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS = 50

#See https://en.wikipedia.org/wiki/Decimal_degrees#Precision
DEFAULT_JOSM_LOAD_RECTANGLE_HALFWIDTH_DEGREES = 0.001

SUGGESTED_JOSM_TRAFFIC_CONDITIONS_VIDEO_CAM_TAGS = {
    'man_made' : 'surveillance',
    'surveillance' : 'public',
    'surveillance:type' : 'camera',
    'surveillance:zone' : 'traffic',
    'camera:type' : 'fixed',
    'camera:mount' : 'pole'
}

SUGGESTED_JOSM_SPEED_CAM_TAGS = {
    'highway':'speed_camera',
}

SUGGESTED_JOSM_AVERAGE_SPEED_CAM_TAGS = {
    'highway':'speed_camera',
}
