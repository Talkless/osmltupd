#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Tuple, Dict
import urllib.parse

import osmltupd.consts

def osm_link_for_node_id(id: int):
    return f'https://www.openstreetmap.org/node/{id}'


def josm_link_for_loading_area(coordinate: Tuple):
    delta = osmltupd.consts.DEFAULT_JOSM_LOAD_RECTANGLE_HALFWIDTH_DEGREES
    left = coordinate[1] - delta
    right = coordinate[1] + delta
    top = coordinate[0] + delta
    bottom = coordinate[0] - delta
    
    return f'http://127.0.0.1:8111/load_and_zoom?left={left}&right={right}&top={top}&bottom={bottom}&zoom_mode=select'


def josm_link_for_loading_node(id: int, coordinate: Tuple):
    delta = osmltupd.consts.DEFAULT_JOSM_LOAD_RECTANGLE_HALFWIDTH_DEGREES
    left = coordinate[1] - delta
    right = coordinate[1] + delta
    top = coordinate[0] + delta
    bottom = coordinate[0] - delta
    
    return f'http://127.0.0.1:8111/load_and_zoom?select=node{id}&left={left}&right={right}&top={top}&bottom={bottom}&zoom_mode=select'


def josm_link_for_adding_node(coordinate: Tuple, keys: Dict):
    kv = [ k + '=' + v for k,v in keys.items()]
    add_tags = urllib.parse.quote('|'.join(kv))
    return f'http://127.0.0.1:8111/add_node?lat={coordinate[0]}&lon={coordinate[1]}&addtags={add_tags}'


def josm_text_tag_list(tags_as_dict: Dict):
    list = [k + '=' + v for k,v in tags_as_dict.items()]
    return '\n'.join(list)
