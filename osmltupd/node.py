#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Tuple

import osmltupd.format

class Node:
    def __init__(self, id, lat, lng):
        self._id = id
        self._lat = lat
        self._lng = lng

    @property
    def id(self):
        return self._id
    
    @property
    def lat(self):
        return self._lat
    
    @property
    def lng(self):
        return self._lng
    
    @property
    def coordinate(self) -> Tuple:
        return (self.lat, self.lng)
    
    def print_plaintext(self):
        self.print_plaintext_header()
        self.print_plaintext_id()
        self.print_plaintext_coordinate()
        self.print_plaintext_additional_fields()
        self.print_plaintext_link()

    def print_plaintext_header(self):
        pass
    
    def print_plaintext_id(self):
        print('id: {}'.format(self.id))
    
    def print_plaintext_coordinate(self):
        print('coordinate: {},{}'.format(self.lat, self.lng))
    
    def print_plaintext_additional_fields(self):
        pass
        
    def print_plaintext_link(self):
        pass
    
    def __repr__(self):
        return '{}(id={}, lat={}, lng={})'.format(type(self).__name__, self.id, self.lat, self.lng)


class OSMNode(Node):
    def __init__(self, id, lat, lng):
        super().__init__(id, lat, lng)

    def print_plaintext_link(self):
        print('OSM link: {}'.format(osmltupd.format.osm_link_for_node_id(self.id)))
