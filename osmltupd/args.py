#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
from enum import Enum

class ReportType(Enum):
    TRAFFIC_CONDITIONS_VIDEO_CAMS_AS_SPEED_CAMS = 'traffic-conditions-video-cams-as-speed-cams'
    MISSING_TRAFFIC_CONDITIONS_VIDEO_CAMS= 'missing-traffic-conditions-video-cams'
    MISSING_SPEED_CAMS = 'missing-speed-cams'
    MISSING_AVERAGE_SPEED_CAMS = 'missing-average-speed-cams'
    REDUNDANT_SPEED_CAMS = 'redundant-speed-cams'

def parse():
    parser = argparse.ArgumentParser(description='Utility for updating OpenStreetMap maps using Lithuania data sources (eismoinfo.lt)')
    
    parser.add_argument('--cache', action='store_true', help='Load OSM data from cache to avoid hitting Overpass-Turbo API query limits')
    
    subparsers = parser.add_subparsers(required=True, dest='report_type', help='Select report to generate')
    
    traffcond_parser = subparsers.add_parser('traffic-conditions-video-cams-as-speed-cams',                                         
                                          description='Generate report on incorrectly mapped traffic conditions monitoring video cameras as speed cameras based on eismoinfo.lt data')
    traffcond_parser.add_argument('--fix-with-josm', action='store_true', help="""Fix problems using JOSM via it's RemoteControl feature one by one""")
    
    missing_traffcond_parser = subparsers.add_parser('missing-traffic-conditions-video-cams',                                         
                                          description='Generate report on missing traffic conditions video cameras based on eismoinfo.lt data') 
    missing_traffcond_parser.add_argument('--fix-with-josm', action='store_true', help="""Fix problems using JOSM via it's RemoteControl feature one by one""")
    
    missing_speedcam_parser = subparsers.add_parser('missing-speed-cams',
                                                    description='Generate report on missing speed cameras in OSM database, based on eismoninfo.lt data')
    missing_speedcam_parser.add_argument('--fix-with-josm', action='store_true', help="""Fix problems using JOSM via it's RemoteControl feature one by one""")
    
    missing_average_speedcam_parser = subparsers.add_parser('missing-average-speed-cams',
                                                    description='Generate report on missing average speed cameras in OSM database, based on eismoninfo.lt data')
    missing_average_speedcam_parser.add_argument('--fix-with-josm', action='store_true', help="""Fix problems using JOSM via it's RemoteControl feature one by one""")
    
    
    redundant_speedcamp_parser = subparsers.add_parser('redundant-speed-cams',
                                                       description='''Generate report on redundant speed cameras in OSM database that are missing in eismoninfo.lt data.'''
                                                       '''WARNING: please use other reports (as "video-cams-as-speed-cams") to check if there are mistakes before removing '''
                                                       '''allegedly "surplus" speed cameras from OSM database!''')
    redundant_speedcamp_parser.add_argument('--fix-with-josm', action='store_true', help="""Fix problems using JOSM via it's RemoteControl feature one by one""")
    
    return parser.parse_args()
