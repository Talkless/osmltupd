#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List, Tuple

import osmltupd.consts
import osmltupd.geo_utils
import osmltupd.format
from osmltupd.node import Node, OSMNode


class OfficialTrafficVideoCam(Node):
    
    def __init__(self, id, lat, lng):
        super().__init__(id, lat, lng)
    
    def print_plaintext_additional_fields(self):
        print('suggested tags:\n{}'.format(osmltupd.format.josm_text_tag_list(osmltupd.consts.SUGGESTED_JOSM_TRAFFIC_CONDITIONS_VIDEO_CAM_TAGS)))
        
    @staticmethod
    def from_official_json(json):
        id = json['attributes']['objectid']
        geo = json['geometry']
        x = geo['x']
        y = geo['y']
        
        # some object has NaN as x & y fields???
        if x == 'NaN' or y == 'NaN':
            return OfficialTrafficVideoCam(None, None, None)
         
        lat, lng = osmltupd.geo_utils.lks94_to_wgs84(x, y)
        return OfficialTrafficVideoCam(id, lat, lng)


class OsmSurveillanceCam(OSMNode):
    
    def __init__(self, id, lat, lng):
        super().__init__(id, lat, lng)
    
    def asdict(self):
        return {'id' : self.id, 'lat' : self.lat, 'lng' : self.lng}
        
    @staticmethod
    def from_overpy_node(node):
        return OsmSurveillanceCam(node.id, float(node.lat), float(node.lon))

    @staticmethod
    def from_json_object(object):
        return OsmSurveillanceCam(object['id'], object['lat'], object['lng'])
    
    @staticmethod
    def list_from_json_cache(json: List):
        return [OsmSurveillanceCam.from_json_object(object) for object in json]
    
    @staticmethod
    def to_json_cache(list: list):
        return [node.asdict() for node in list]
