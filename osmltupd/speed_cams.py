#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List, Tuple

import osmltupd.geo_utils
from osmltupd.node import Node, OSMNode


class OfficialSpeedCam(Node):
    
    def __init__(self, id, lat, lng, speed, distance, range_title=None):
        super().__init__(id, lat, lng)
        self._speed = speed
        self._range_title = range_title 
        self._distance = distance

    @property
    def speed(self):
        return self._speed
    
    @property
    def distance(self):
        return self._distance
    
    @property
    def range_title(self):
        return self._range_title
    
    def print_plaintext_additional_fields(self):
        if self.speed:
            print('speed: {}'.format(self.speed))
        if self.distance:
            print('distance: {}km'.format(self.distance))
    
    @staticmethod
    def from_official_json(json):   
        attributes = json['attributes'] 
        id = attributes['objectid']
        speed = attributes.get('greitis', None)
        distance = attributes['vietakm']
        range_title = attributes.get('pavadinimas') 
        geo = json['geometry']
        lat, lng = osmltupd.geo_utils.lks94_to_wgs84(geo['x'], geo['y'])
        return OfficialSpeedCam(id, lat, lng, speed, distance, range_title)
    
    def __repr__(self):
        return '{}(id={}, lat={}, lng={}, speed={}, distance={}, range_title={})'.format(type(self).__name__, self.id, self.lat, self.lng, self.speed, self.distance, self.range_title)


class OsmSpeedCam(OSMNode):
    
    def __init__(self, id, lat, lng):
        super().__init__(id, lat, lng)
    
    def asdict(self):
        return {'id' : self.id, 'lat' : self.lat, 'lng' : self.lng}
        
    @staticmethod
    def from_overpy_node(node):
        return OsmSpeedCam(node.id, float(node.lat), float(node.lon))

    @staticmethod
    def from_json_object(object):
        return OsmSpeedCam(object['id'], object['lat'], object['lng'])
    
    @staticmethod
    def list_from_json_cache(json: List):
        return [OsmSpeedCam.from_json_object(object) for object in json]
    
    @staticmethod
    def to_json_cache(list: list):
        return [node.asdict() for node in list]
