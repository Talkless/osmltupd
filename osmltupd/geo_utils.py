#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from math import degrees, pi, pow, radians, sin, cos, sqrt, tan
from typing import Tuple

from great_circle_calculator.great_circle_calculator import distance_between_points

def distance(point_a, point_b):
    return distance_between_points(point_a, point_b)


# Converted to Python from https://github.com/vilnius/lks2wgs/blob/master/wgs2lks.php
def lks94_to_wgs84(x, y) -> Tuple:
    
    #TODO: move constants outside of function?
    distsize = 3
    j = 0
    units = 1
    k = 0.9998
    a = 6378137
    f = 1 / 298.257223563
    b = a * (1 - f)
    e2 = (a * a - b * b) / (a * a)
    e = sqrt(e2)
    ei2 = (a * a - b * b) / (b * b)
    ei = sqrt(ei2)
    n = (a - b) / (a + b)
    G = a * (1 - n) * (1 - n * n) * (1 + (9 / 4) * n * n + (255 / 64) * pow(n, 4)) * (pi / 180)
    north = (y - 0) * units
    east = (x - 500000) * units
    m = north / k
    sigma = (m * pi) / (180 * G)
    footlat = sigma + ((3 * n / 2) - (27 * pow(n, 3) / 32)) * sin(2 * sigma) + ((21 * n * n / 16) - (55 * pow(n, 4) / 32)) * sin(4 * sigma) + (151 * pow(n, 3) / 96) * sin(6 * sigma) + (1097 * pow(n, 4) / 512) * sin(8 * sigma)
    rho = a * (1 - e2) / pow(1 - (e2 * sin(footlat) * sin(footlat)), (3 / 2))
    nu = a / sqrt(1 - (e2 * sin(footlat) * sin(footlat)))
    psi = nu / rho
    t = tan(footlat)
    x = east / (k * nu)
    
    laterm1 = (t / (k * rho)) * (east * x / 2)
    laterm2 = (t / (k * rho)) * (east * pow(x, 3) / 24) * (-4 * psi * psi + 9 * psi * (1 - t * t) + 12 * t * t )
    laterm3 = (t / (k * rho)) * (east * pow(x, 5) / 720) * (8 * pow(psi, 4) * (11 - 24 * t * t) - 12 * pow(psi, 3) * (21 - 71 * t * t) + 15 * psi * psi * (15 - 98 * t * t + 15 * pow(t, 4)) + 180 * psi * (5 * t * t - 3 * pow(t, 4)) + 360 * pow(t, 4))
    laterm4 = (t / (k * rho)) * (east * pow(x, 7) / 40320) * (1385 + 3633 * t * t + 4095 * pow(t, 4) + 1575 * pow(t, 6));
    latrad = footlat - laterm1 + laterm2 - laterm3 + laterm4

    lat_deg = degrees(latrad)

    seclat = 1 / cos(footlat)
    loterm1 = x * seclat
    loterm2 = (pow(x, 3) / 6) * seclat * (psi + 2 * t * t)
    loterm3 = (pow(x, 5) / 120) * seclat * (-4 * pow(psi, 3) * (1 - 6 * t * t) + psi * psi * (9 - 68 * t * t) + 72 * psi * t * t + 24 * pow(t, 4))
    loterm4 = (pow(x, 7) / 5040) * seclat * (61 + 662 * t * t + 1320 * pow(t, 4) + 720 * pow(t, 6))
    w = loterm1 - loterm2 + loterm3 - loterm4
    longrad = radians(24) + w

    lon_deg = degrees(longrad)

    return (lat_deg, lon_deg)