#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys

# add parent directory into path to make "import osmltupd.x" work
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))

import osmltupd.args
import osmltupd.run as run

from osmltupd.args import ReportType


def main():

    arguments = osmltupd.args.parse()
    report_type = arguments.report_type

    if report_type == ReportType.TRAFFIC_CONDITIONS_VIDEO_CAMS_AS_SPEED_CAMS.value:
        
        run.run_traffic_conditions_video_cams_as_speed_cams_command(arguments)
        
    elif report_type == ReportType.MISSING_TRAFFIC_CONDITIONS_VIDEO_CAMS.value:
        
        run.run_missing_traffic_conditions_video_cams_command(arguments)
        
    elif report_type == ReportType.MISSING_SPEED_CAMS.value:
        
        run.run_missing_speed_cams_command(arguments)
        
    elif report_type == ReportType.MISSING_AVERAGE_SPEED_CAMS.value:
        
        run.run_missing_average_speed_cams_command(arguments)
        
    elif report_type == ReportType.REDUNDANT_SPEED_CAMS.value:
        
        run.run_redundant_speed_cams_command(arguments)

if __name__ == '__main__':
    main()
