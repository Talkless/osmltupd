#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import json
import tempfile

from typing import Callable

import overpy
import requests

import osmltupd.consts
from osmltupd.speed_cams import OfficialSpeedCam, OsmSpeedCam
from osmltupd.video_cams import OfficialTrafficVideoCam, OsmSurveillanceCam

def __fetch_or_load_cache(use_cache, cache_file_name : str, fetch_function: Callable, load_function : Callable):

    cache_file_path = os.path.join(tempfile.gettempdir(), cache_file_name)
    cache = None
    if (use_cache and os.path.exists(cache_file_path)):
        print("Loading OSM data from local cache file '{}' instead...".format(cache_file_path))
        with open(cache_file_path, 'r') as f:
            cache = json.loads(f.read())
            return load_function(cache)
    else:
        list = fetch_function()
        with open(cache_file_path, 'w') as f:
            print("Saving OSM data into cache file '{}'...".format(cache_file_path))
            json_data = OsmSpeedCam.to_json_cache(list)
            f.write(json.dumps(json_data))
        return list


def fetch_lithuanian_speed_camera_nodes_from_osm_cache(cached_json):
    return OsmSpeedCam.list_from_json_cache(cached_json)


def fetch_lithuanian_speed_camera_nodes_from_osm_or_cache(use_cache):
    return __fetch_or_load_cache(use_cache, osmltupd.consts.OSM_SPEED_CAMERA_CACHE_FILE_NAME, fetch_lithuanian_speed_camera_nodes_from_osm, fetch_lithuanian_speed_camera_nodes_from_osm_cache)


def fetch_lithuanian_speed_camera_nodes_from_osm():
    api = overpy.Overpass()

    # fetch all ways and nodes
    result = api.query("""
        area[name="Lietuva"];
         (node["highway"="speed_camera"](area););

        out meta;
        """)
    
    return [OsmSpeedCam.from_overpy_node(node) for node in result.nodes]

def fetch_lithuanian_surveillance_camera_nodes_from_osm():
    api = overpy.Overpass()

    # fetch all ways and nodes
    result = api.query("""
        area[name="Lietuva"];
         (node["man_made"="surveillance"]
              ["surveillance:type"="camera"]
              (area););

        out meta;
        """)
    
    return [OsmSurveillanceCam.from_overpy_node(node) for node in result.nodes]


def fetch_lithuanian_surveillance_camera_nodes_from_osm_cache(cached_json):
    return OsmSurveillanceCam.list_from_json_cache(cached_json)


def fetch_lithuanian_traffic_surveillance_camera_nodes_from_osm_or_cache(use_cache):
    return __fetch_or_load_cache(use_cache, osmltupd.consts.OSM_SURVEILLANCE_VIDEO_CAMS_CACHE_FILE_NAME, fetch_lithuanian_surveillance_camera_nodes_from_osm, fetch_lithuanian_surveillance_camera_nodes_from_osm_cache)


def fetch_lithuanian_speed_cameras_from_eismoinfo_lt():
    """
    Download JSON from eismoinfo.lt with list of speed cameras in Lithuania, return list of {lat, lng} objects.
    """
    r = requests.get(osmltupd.consts.EISMOINFO_SPEED_CAMERA_JSON_URL)
    json_result = r.json()
    features = json_result['features']
    return [OfficialSpeedCam.from_official_json(json) for json in features]


def fetch_lithuanian_average_speed_cameras_from_eismoinfo_lt():
    """
    Download JSON from eismoinfo.lt with list of average speed cameras in Lithuania, return list of {lat, lng} objects.
    """
    r = requests.get(osmltupd.consts.EISMOINFO_AVERAGE_SPEED_CAMERA_JSON_URL)
    json_result = r.json()
    features = json_result['features']
    return [OfficialSpeedCam.from_official_json(json) for json in features]


def fetch_lithuanian_traffic_conditions_video_cameras_from_eismoinfo_lt():
    """
    Download JSON from eismoinfo.lt with list of traffic video cameras in Lithuania, return list of {lat, lng} objects.
    """
    r = requests.get(osmltupd.consts.EISMOINFO_TRAFFIC_CONDITIONS_VIDEO_CAMERA_JSON_URL)
    json_result = r.json()
    features = json_result['features']
    list = [OfficialTrafficVideoCam.from_official_json(json) for json in features]
    # Remove cameras with 'NaN' x/y coordinates.
    return [cam for cam in list if cam.lat != None and cam.lng != None]

