#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List

import osmltupd.geo_utils

def find_missing_nodes_within_distance_threshold(needles: List, haystack: List, distance_meters: int) -> List:
    missing_nodes = list()
    
    for haystack_item in haystack:        
        found = False
        
        for needle in needles:
            distance = osmltupd.geo_utils.distance(haystack_item.coordinate, needle.coordinate)
            if distance <= distance_meters:
                found = True
                continue
        
        if not found:
            missing_nodes.append(haystack_item)
            
    return missing_nodes
