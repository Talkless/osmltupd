#  This file is part of OsmLTupd command-line utilty for updating 
#  OpenStretMap data using Lithuanian data sources.
#
#  Copyright (c) 2022 Vincas Dargis <vindrg@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List, Type
import itertools

import osmltupd.algorithm
import osmltupd.consts
import osmltupd.geo_utils
from osmltupd.speed_cams import OsmSpeedCam
from osmltupd.video_cams import OfficialTrafficVideoCam

class AverageSpeedRange:
    
    def __init__(self, first: Type[OfficialTrafficVideoCam], second: Type[OfficialTrafficVideoCam]):
        self._first = first
        self._second = second
        
    @property
    def first(self):
        return self._first
    
    @property
    def second(self):
        return self._second

    def __repr__(self):
        return 'AverageSpeedRange(frist={}, second={})'.format(self.first, self.second)


AverageSpeedRangeList = List[AverageSpeedRange]

class MissingAvedrageSpeedCamReportItem:
    def __init__(self, range: Type[AverageSpeedRange], first_missing: bool, second_missing: bool):
        self._range = range
        self._first_missing = first_missing
        self._second_missing = second_missing
        
    @property
    def range(self):
        return self._range
    
    @property
    def first_missing(self) -> bool:
        return self._first_missing
   
    @first_missing.setter
    def first_missing(self, value : bool):
        self._first_missing = value
        
    @property
    def second_missing(self) -> bool:
        return self._second_missing

    @second_missing.setter
    def second_missing(self, value : bool):
        self._second_missing = value
        
    def print_plaintext(self):
        print('range title: {}'.format(self.range.first.range_title))
        print('-- first camera --')
        print('missing: {}'.format(self.first_missing))
        if self.first_missing:
            self.range.first.print_plaintext()
        else:
            print(self.range.first.print_plaintext_id())
            print(self.range.first.print_plaintext_coordinate())
        print('-- second camera --')
        print('missing: {}'.format(self.second_missing))
        if self.second_missing:
            self.range.second.print_plaintext()
        else:
            print(self.range.second.print_plaintext_id())
            print(self.range.second.print_plaintext_coordinate())
                
    def __repr__(self):
        print('MissingAvedrateSpeedCamReportItem(first_missing={}, second_missing={}, range={}'.format(self.first_missing, self.second_missing, self.range))

class IncorrectlyTaggedVideoCamReportItem:
    
    def __init__(self, official_video_cam: Type[OfficialTrafficVideoCam], osm_speed_cam: Type[OsmSpeedCam]):
        self._official_match = official_video_cam
        self._osm_match = osm_speed_cam
    
    @property
    def official_match(self):
        return self._official_match
    
    @property
    def osm_match(self):
        return self._osm_match
    
    def print_plaintext(self):
        print('-- OSM camera --')
        self.osm_match.print_plaintext()
        print('-- Official camera --')
        self.official_match.print_plaintext()
    
    def __repr__(self):
        return 'IncorrectlyTaggedVideoCamReportItem(official_match={}, osm_match={})'.format(self.official_match, self.osm_match)


def __group_average_speed_ranges_by_title(official_average_speed_camera_list: List) -> AverageSpeedRangeList:
    sorted_cams = sorted(official_average_speed_camera_list, key=lambda i: (i.range_title, i.distance))
    grouped_ranges = []
    for k, items in itertools.groupby(sorted_cams, key=lambda i: i.range_title):
        item_list = list(items) #dereference iteratables
        # group [1, 2, 3, 4, ...] into [1, 2], [3, 4], ..., as some roads have multiple average speed measurement ranges
        for first, second in zip(item_list[::2], item_list[1::2]): 
            grouped_ranges.append(AverageSpeedRange(first, second))
    return grouped_ranges
            

def report_incorrectly_tagged_traffic_conditions_video_cams_as_speed_cams_in_osm(official_traffic_video_camera_list: List, official_speed_camera_list: List, official_average_speed_camera_list: List, osm_camera_list: List) -> List:
    
    incorrectly_tagged = list()
    
    
    def coordinate_matches_in_list(coordinate, list):
        for item in list:
            distance = osmltupd.geo_utils.distance(coordinate, item.coordinate)
            if distance < osmltupd.consts.DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS:
                return True
        return False

    #We avoid false positives where OSM has CORRECLTY mapped speed cameras, that might 
    # be near surveillance camera, that would then "match" incorrectly in this report due to big threshold.
    cleaned_osm_camera_list = [item for item in osm_camera_list
    if not coordinate_matches_in_list(item.coordinate, official_speed_camera_list) 
    and not coordinate_matches_in_list(item.coordinate, official_average_speed_camera_list)]
        
    for osm_cam in cleaned_osm_camera_list:        
        found = False
        
        for off_cam in official_traffic_video_camera_list:
            distance = osmltupd.geo_utils.distance(off_cam.coordinate, osm_cam.coordinate)
            if distance <= osmltupd.consts.DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS:
                incorrectly_tagged.append(IncorrectlyTaggedVideoCamReportItem(off_cam, osm_cam))
                continue
        
    return incorrectly_tagged


def report_missing_speed_cams_in_osm(official_list: List, osm_list: List) -> List:
    return osmltupd.algorithm.find_missing_nodes_within_distance_threshold(osm_list, official_list, osmltupd.consts.DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS)


def report_missing_average_speed_cams_in_osm(official_list: List, osm_list: List) -> List[MissingAvedrageSpeedCamReportItem]:
    missing_cams = osmltupd.algorithm.find_missing_nodes_within_distance_threshold(osm_list, official_list, osmltupd.consts.DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS)
    missing_cams_id_set = { i.id for i in missing_cams}
    grouped_ranges = __group_average_speed_ranges_by_title(official_list)
    missing_or_incomplete_ranges = []
    for range in grouped_ranges:
        first_missing = range.first.id in missing_cams_id_set 
        second_missing = range.second.id in missing_cams_id_set
        if first_missing or second_missing:
            missing_or_incomplete_ranges.append(MissingAvedrageSpeedCamReportItem(range, first_missing, second_missing))
    
    return missing_or_incomplete_ranges    


def report_redundant_speed_cams_in_osm(official_instant_cam_list: List, official_average_cam_list: List, osm_list: List) -> List:
    return osmltupd.algorithm.find_missing_nodes_within_distance_threshold(official_instant_cam_list + official_average_cam_list, osm_list, osmltupd.consts.DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS)


def report_missing_traffic_conditions_video_cameras_in_osm(official_list : List, osm_list : List) -> List:
    return osmltupd.algorithm.find_missing_nodes_within_distance_threshold(osm_list, official_list, osmltupd.consts.DEFAULT_MATCHING_DISTANCE_THRESHOLD_METERS)
