# OsmLTupd

Command-line utility for updating OpenStreetMap maps using Lithuanian data sources.

Currently supported sources:

* https://eismoinfo.lt (OpenData portal providing information about speed cameras (both instant and average speed), also cameras monitoring traffic conditions. 

## Installation

### Linux

Simply create virtual environment and activate it with helper script provided:
```
source ./venv.sh
```

## Usage

Invoking general help, list of available modes:

```
python3 osmltupd --help
```

### "Traffic Condition Video Cams As Speed Cams" mode

General help about this mode:
```
python3 osmltupd traffic-conditions-video-cams-as-speed-cams --help
```

To find cameras incorrectly mapped as speed cameras when in reality these are traffic condition cameras (mapping mistakes):

```
python3 osmltupd traffic-conditions-video-cams-as-speed-cams
```
```
Fetching Lithuanian speed cameras from OSM...
Loading OSM data from local cache file '/tmp/osmltupd_osm_speed_cameras_cache.json' instead...
Fetching Lithuanian traffic conditions video cameras from eismoinfo.lt...
Fetching Lithuanian speed cameras from eismoinfo.lt...
Fetching Lithuanian average speed cameras from eismoinfo.lt...
Total official video camera count: 130
Total OSM camera count: 338
Generating traffic conditions video cams as speed cams mistake report...
Incorrectly tagged video cameras as speed camera count: 0
```
To initiate manual fixing procedure with JOSM, add `--fix-with-josm`:

```
python3 osmltupd traffic-conditions-video-cams-as-speed-cams --fix-with-josm
```

### "Missing Traffic Conditions Video Cams" mode

General help about this mode:
```
python3 osmltupd missing-traffic-conditions-video-cams --help
```

To find traffic condition cameras missing from OpenStreetMap database, but available in eismoinfo.lt:

```
python3 osmltupd missing-traffic-conditions-video-cams
```
```
Fetching Lithuanian traffic conditions (traffic surveillance) video cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_surveillance_video_cameras_cache.json'...
Fetching Lithuanian traffic conditions video cameras from eismoinfo.lt...
Total official video camera count: 130
Total OSM camera count: 404
Generating missing traffic conditions video cameras report...
Missing traffic conditions video camera count: 1
Missing traffic conditions video cameras:
------------------------------------------
id: 1137
coordinate: 54.569348597900486,25.31597525135032
suggested tags:
man_made=surveillance
surveillance=public
surveillance:type=camera
surveillance:zone=traffic
camera:type=fixed
camera:mount=pole
------------------------------------------
```
To initiate manual fixing procedure with JOSM, add `--fix-with-josm`:

```
python3 osmltupd missing-traffic-conditions-video-cams --fix-with-josm
```
```
Fetching Lithuanian traffic conditions (traffic surveillance) video cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_surveillance_video_cameras_cache.json'...
Fetching Lithuanian traffic conditions video cameras from eismoinfo.lt...
Total official video camera count: 130
Total OSM camera count: 404
Generating missing traffic conditions video cameras report...
Missing traffic conditions video camera count: 1
================
Adding 1/1 camera
Official id: 1137
Official coordinates: 54.569348597900486,25.31597525135032
JOSM add url: http://127.0.0.1:8111/add_node?lat=54.569348597900486&lon=25.31597525135032&addtags=man_made=surveillance|surveillance=public|surveillance:type=camera|surveillance:zone=traffic|camera:type=fixed|camera:mount=pole
Press Enter to continue...

```

### "Missing Speed Cams" mode

General help about this mode:
```
python3 osmltupd missing-speed-cams --help
```
To find missing speed cameras from OpenStreetMap database, but available in eismoinfo.lt:

```
python3 osmltupd missing-speed-cams
```
```
Fetching Lithuanian speed cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_speed_cameras_cache.json'...
Fetching Lithuanian speed cameras from eismoinfo.lt...
Total OSM camera count: 338
Official speed camera count: 70
Generating missing camera report...
Missing camera count: 1
Missing cameras:
------------------------------------------
id: 225
coordinate: 54.76511938430501,23.723801612988048
speed: 50
distance: 4.217km
------------------------------------------
```

To initiate manual fixing procedure with JOSM, add `--fix-with-josm`:

```
python3 osmltupd missing-speed-cams --fix-with-josm
```
```
Fetching Lithuanian speed cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_speed_cameras_cache.json'...
Fetching Lithuanian speed cameras from eismoinfo.lt...
Total OSM camera count: 338
Official speed camera count: 70
Generating missing camera report...
Missing camera count: 1
================
Adding 1/1 camera
Official id: 225
Official coordinates: 54.76511938430501,23.723801612988048
Official speed: 50
JOSM add url: http://127.0.0.1:8111/add_node?lat=54.76511938430501&lon=23.723801612988048&addtags=highway=speed_camera|maxspeed=50
```

### "Missing Average Speed Cams" mode

General help about this mode:
```
python3 osmltupd missing-average-speed-cams --help
```
To find missing average speed cameras from OpenStreetMap database, but available in eismoinfo.lt:

```
python3 osmltupd missing-average-speed-cams
```
```
Fetching Lithuanian average speed cameras from eismoinfo.lt...
Total OSM speed camera (instantenious and average speed) count: 338
Official average speed camera count: 162
Generating missing average speed camera report...
Missing or incomplete average speed camera range count: 1
Missing or incomplete average speed camera ranges:
------------------------------------------
range title: Šiauliai–Palanga
-- first camera --
missing: True
id: 294
coordinate: 56.00082464923835,22.556446806688424
distance: 49.55km
-- second camera --
missing: True
id: 253
coordinate: 56.000383636341745,22.464660684339634
distance: 55.37km
------------------------------------------
```

To initiate manual fixing procedure with JOSM, add `--fix-with-josm`:

```
python3 osmltupd missing-average-speed-cams --fix-with-josm
```
```
Fetching Lithuanian speed cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_speed_cameras_cache.json'...
Fetching Lithuanian average speed cameras from eismoinfo.lt...
Total OSM speed camera (instantenious and average speed) count: 338
Official average speed camera count: 162
Generating missing average speed camera report...
Missing or incomplete average speed camera range count: 1
================
Fixing 1/1 average speed range
Official range title: Šiauliai–Palanga
-- First camera --
Official id: 253
Official coordinates: 56.00082464923835,22.556446806688424
JOSM add url: http://127.0.0.1:8111/add_node?lat=56.00082464923835&lon=22.556446806688424&addtags=highway=speed_camera
-- Second camera --
Official id: 253
Official coordinates: 56.000383636341745,22.464660684339634
JOSM add url: http://127.0.0.1:8111/add_node?lat=56.000383636341745&lon=22.464660684339634&addtags=highway=speed_camera
Press Enter to continue...
```

### "Redundant Speed Cams" mode

General help about this mode:
```
python3 osmltupd redundant-speed-cams --help
```
**WARNING** *DO NOT* use this mode to actually remove allegedly "redundant" cameras from OpenStreetMap using list produced in this mode. Currently, not all speed cameras are available in eismoinfo.lt database. Only cameras in national-importance roads are expected to be there, while regional (within local country, city) cameras are not necessarily reported into eismoinfo.lt database (source: eismoinfo.lt representative contacted via email).

To find redundant speed cameras in OpenStreetMap database that are missing from eismoinfo.lt:

```
python3 osmltupd redundant-speed-cams
```
```
Fetching Lithuanian speed cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_speed_cameras_cache.json'...
Fetching Lithuanian instant speed cameras from eismoinfo.lt...
Fetching Lithuanian average speed cameras from eismoinfo.lt...
Generating redundant camera report (please fix "traffic-conditions-video-cams-as-speed-cams" mistakes first!)...
Possibly redundant camera count: 106
WARNING: Not all cameras are available in eismoinfo.lt!
WARNING: Use this as a hint only for roads of national importance! Check Mapillary/Kartaview if possible before removing the camera!
Possibly redundant cameras:
------------------------------------------
id: 303226673
coordinate: 54.6741186,25.2686582
OSM link: https://www.openstreetmap.org/node/303226673
------------------------------------------
id: 303226812
coordinate: 54.6778005,25.267461
OSM link: https://www.openstreetmap.org/node/303226812
------------------------------------------
...
```

To initiate manual fixing procedure with JOSM, add `--fix-with-josm`:

```
python3 osmltupd redundant-speed-cams --fix-with-josm
```
```
Fetching Lithuanian speed cameras from OSM...
Saving OSM data into cache file '/tmp/osmltupd_osm_speed_cameras_cache.json'...
Fetching Lithuanian instant speed cameras from eismoinfo.lt...
Fetching Lithuanian average speed cameras from eismoinfo.lt...
Generating redundant camera report (please fix "traffic-conditions-video-cams-as-speed-cams" mistakes first!)...
Possibly redundant camera count: 106
WARNING: Not all cameras are available in eismoinfo.lt!
WARNING: Use this as a hint only for roads of national importance! Check Mapillary/Kartaview if possible before removing the camera!
Viewing 1/106 redundant camera
OSM id: 303226673
OSM url: https://www.openstreetmap.org/node/303226673
JOSM load url: http://127.0.0.1:8111/load_and_zoom?select=node303226673&left=25.2676582&right=25.269658200000002&top=54.6751186&bottom=54.6731186&zoom_mode=select
Press Enter to continue...
```

### Caching

OsmLTupd always saves data fetched from Overpass-Turbo server into temporary file. It is recommended to use this cache when generating reports again and again using this tool to reduce load on Overpass-Turbo infrastructure.

To use cached nodes prepend `--cache` *before* choosing mode:

```
python3 osmltupd --cache redundant-speed-cams --fix-with-josm
```
```
Fetching Lithuanian speed cameras from OSM...
Loading OSM data from local cache file '/tmp/osmltupd_osm_speed_cameras_cache.json' instead...
Fetching Lithuanian instant speed cameras from eismoinfo.lt...
Fetching Lithuanian average speed cameras from eismoinfo.lt...
Generating redundant camera report (please fix "traffic-conditions-video-cams-as-speed-cams" mistakes first!)...
Possibly redundant camera count: 106
WARNING: Not all cameras are available in eismoinfo.lt!
WARNING: Use this as a hint only for roads of national importance! Check Mapillary/Kartaview if possible before removing the camera!
Viewing 1/106 redundant camera
OSM id: 303226673
OSM url: https://www.openstreetmap.org/node/303226673
JOSM load url: http://127.0.0.1:8111/load_and_zoom?select=node303226673&left=25.2676582&right=25.269658200000002&top=54.6751186&bottom=54.6731186&zoom_mode=select
Press Enter to continue...
```


## License

This software is licensed under GPLv3 (see COPYING).

NOTE: `lks94_to_wgs84` function in `geo_utils.py` is converted from `https://github.com/vilnius/lks2wgs/blob/master/wgs2lks.php` with original license undefined.
